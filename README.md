# Bot Against Humanity
*Simple __chaotic__ Mastodon bot written in python*

### Requirements
- Python 3 (tested with v3.7.3)
- Packages listed in `requirements.txt`
- A Mastodon account (tested against Mastodon 2.7.4)

### Usage
Create a `localconfig.py` in the tree root. Please take a look at the sample.
- Run `bot.py`, it will check for new notifications and answer them.
OR
- Run `bot.py <command>` to post a status.

##### Get answers from bot
Just mention the bot account from another one with the correct syntax:

    SYNOPSIS: "@bot command"
    
    COMMANDS:
    nah                 scrambled news
    <other words>       get a smart reply

If the `bot.py` script is (periodically) executed, it will find the metion and then it answers you. The visibility is reflected from the original mention.

##### Post statuses from command line
Just run the script with the correct syntax and you will post a status from the bot account:

    $ python3 bot.py <command>

### Notes
- __news against humanity__: Review the `nah` python function to match the tree of the xml feed of your choice.
