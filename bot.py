#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re
import requests
import json
import random
import datetime
import pysnooper
import feedparser
from mastodon import Mastodon
from bs4 import BeautifulSoup

import localconfig as config

version = "0.4"
mastodon = Mastodon(access_token = config.token, api_base_url = config.base_url)
ack_nots = config.acknowledged_notifications
date_limit = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(days = config.max_notifications_days)
shortacc = config.short_account
shell_prefix = "shell"
newsfeeds = config.newsfeeds
newspref = config.newspref
news_nouns_re = config.news_nouns_re

def htmlsan(html):
    soup = BeautifulSoup(html, features="lxml")
    text = soup.get_text()
    return text

def uppercase_first(string):
    text = string[0].upper() + string[1:]
    return text

#@pysnooper.snoop()
def new_notifications(nots, ack_nots):
    "Return a list with new notfications"
    new_nots = list()
    for n in nots:
        try:
            nid = str(n['status']['id'])
            with open(ack_nots, 'r+') as acks:
                if not any(nid == x.rstrip('\r\n') for x in acks):
                    new_nots.append(n)
                    print(nid, file = acks)
        except:
            pass
    return new_nots


def websearch(query, n):
    "Search the WWW with the given query"
    try:
        url = config.websearch_api + query
        response = requests.get(url, headers=config.websearch_headers).json()
        result = response['data']['result']['items'][n]['url']
        return result
    except:
        pass


#@pysnooper.snoop()
def nah():
    "News Against Humanity"
    try:
        news = list()
        shards = list()
        ans = list()
        quest = list()
        re_nouns = re.compile(news_nouns_re, re.IGNORECASE)
        blank = "_____"

        for feed in newsfeeds:
            news += feedparser.parse(feed)['entries']

        for entry in news:
            #shards += htmlsan(entry['title']).split('.')
            shards += htmlsan(entry['summary']).split('.')

        phrases = list(filter(None, shards))
        phrases[:] = [x for x in phrases if x != ' ']

        for fr in phrases:
            a_list = re.findall(re_nouns, fr)
            for a in a_list:
                ans.append(a)
            quest.append(re.sub(re_nouns, blank, fr))

        rpl = lambda _:"%s" % random.choice(ans)

        result = newspref
        for cycle in range(config.news_cycles):
            result += uppercase_first(re.sub(blank, rpl, random.choice(quest)).strip()) + '. '

        text = re.sub(r' {2,}', ' ', result)
        return text
    except:
        pass


#@pysnooper.snoop()
def besmart(url):
    "Find a suitable phrase on the internet"

    try:
        res = requests.get(url)
        html_page = res.content
        soup = BeautifulSoup(html_page, 'html.parser')
        text = soup.find_all(text=True)
        output = ''
        bad_tags = config.smart_blacklist_tags
        
        for t in text:
            if t.parent.name not in bad_tags:
                output += '{} '.format(t)

        sentence_list_re = re.compile(config.smart_sentences_re)
        bad_words = re.compile(config.smart_blacklist_words, re.IGNORECASE)
        sentence_list = re.findall(sentence_list_re, output)
        sentence_list_filtered = [item for item in sentence_list if not bad_words.match(item)]
        return random.choice(sentence_list_filtered)

    except:
        pass


#@pysnooper.snoop()
def answer(nn):
    "Answer notification, based on message content"

    try:
        qsep = "+"
        tsep = " "
        nid = str(nn['status']['id'])
        message_from = "@" + str(nn['status']['account']['acct'])
        message_vis = str(nn['status']['visibility'])
        date_created = nn['status']['created_at']
        content = htmlsan(re.sub('<br( */)*>', ' ', str(nn['status']['content'])))
        mention = tsep.join(re.findall(r'\b@\S+', content))
        mention_notme = re.sub(r"\b%s\S*" % shortacc, ' ', mention).strip()
        message_text = re.sub(r'\b@\S+', ' ', content).strip()
        message_args = message_text.split()
        command = message_args[0]

        # check match for given command and create a status
        if re.search(r"^nah$", command, re.IGNORECASE):
            status = nah()
        elif re.search(r"^[a-z0-9]+$", command, re.IGNORECASE):
            words = re.findall(r'\b\w{%s,}\b' % config.smart_word_minsize, message_text)
            query = qsep.join(words)
            r = random.randint(0, 19)
            url = websearch(query, r)
            status = besmart(url)
 
        # if a status is returned
        if status:
            status_trunc = status[:config.max_chars]

            # if mention is our accont
            # => reply to notification
            if re.search(r"\b%s\S*" % shortacc, mention, re.IGNORECASE):
                reply = message_from + "\n" + status_trunc + "\n" + mention_notme
                mastodon.status_post(reply, visibility = message_vis, in_reply_to_id = nid)

            # if message is from shell
            # => post a public toot
            elif re.search(r"@%s$" % shell_prefix, message_from, re.IGNORECASE):
                print(content)
                print("{0} ({1} from {2}): {3}".format(nid, message_vis, message_from, status_trunc))
                mastodon.status_post(status_trunc, visibility = message_vis)

    except:
        pass

#@pysnooper.snoop()
def main():
    args = sys.argv
    # args == 1,  => no command given (check and answer notification if any)
    if len(args) == 1:

        nots = mastodon.notifications(limit = config.max_notifications)
        new_nots = new_notifications(nots, ack_nots)

        try:
           open(ack_nots, 'x')
        except FileExistsError:
           pass

        for nn in new_nots:
            date_created = nn['status']['created_at']

            if date_created > date_limit:
                answer(nn)

    # args >1 (probably 2) => a command is given
    elif len(args) > 1:
        del args[0]
        sep = " "
        content = sep.join(args)
        nn = {'status': {'id': 0, 'account': {'acct': 'shell'}, 'visibility': 'public', 'created_at': 0, 'content': content}}
        answer(nn)


if __name__ == "__main__":
    main()
