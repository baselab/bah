#!/usr/bin/env bash

cd $(dirname $(realpath -- "${0}"))
. ../bin/activate #if installed in virtualenv
python3 bot.py ${*}
